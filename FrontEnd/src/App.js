import React from 'react';
import './App.css';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import Sesion from './fragment/Sesion';
import Inicio from './fragment/Inicio';
import PresentarAuto, { Disponibles, Vendidos } from './fragment/PresentarAuto';
import { estaSesion } from './utilidades/Sessionutil';
import { Mecanica } from './fragment/Consecionario';

function App() {
  const Middeware = ({children}) =>{
    const autenticado = estaSesion();
    const location = useLocation();
    if(autenticado){
      return children;
    }else{
      return <Navigate to= '/sesion' state={location}/>;
    }
  }


  const MiddewareSesion = ({children}) =>{
    const autenticado = estaSesion();
    const location = useLocation();
    if(autenticado){
      return <Navigate to= '/inicio'/>;
      
    }else{
      return children;
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path='/sesion' element={<MiddewareSesion><Sesion/></MiddewareSesion>}/>
        <Route path='/inicio' element={<Middeware><Inicio/></Middeware>}/>
        <Route path='/autos/disponibles' element={<Disponibles/>}/>
        <Route path='/autos/vendidos' element={<Middeware><Vendidos/></Middeware>}/>
        <Route path='/mecanica' element={<Middeware><Mecanica/></Middeware>}/>
      </Routes>
      </div>
  );
}

export default App;
