import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarAuto from "./RegistrarAuto";
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { AutosMecanica, GuardarFacturaServicio, GuardarFacturaServio, GuardarOrdenTrabajo, ObtenerOrden, ObtenerRepuestos } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";

const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;

export const Mecanica = () => {
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llauto, setLlauto] = useState(false);
    const [showModalE, setShowModalE] = useState(false);
    const [showModalS, setShowModalS] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);

    //Entrada a consecionario
    const [fecha, setFecha] = useState(new Date());
    const [descripcion, setDescripcion] = useState('');
    const [presupuesto, setPresupuesto] = useState(0);


    //Salida de consecionario
    const [cantidadRepuestosF, setCantidadRepuestosF] = useState(1);
    const [selectedValues, setSelectedValues] = useState([]);
    const [manoObraF, setManoObraF] = useState(0);
    const [descripcionF, setDescripcionF] = useState('');
    const [subtotalF, setSubtotalF] = useState(0);
    const [totalF, setTotalF] = useState(0);
    const [numeroRepuestosF, setNumeroRepuestosF] = useState([]);

    const [repuestos, setRepuestos] = useState([]);
    const [llrepuestos, setLlrepuestos] = useState(false);

    const handleButtonClickE = (item) => {
        setSelectedItem(item);
        setShowModalE(true);
    };

    const handleButtonClickS = (item) => {
        setSelectedItem(item);
        setShowModalS(true);
    };

    const closeModalE = () => {
        setShowModalE(false);
        setSelectedItem(null);
    };

    const closeModalS = () => {
        setShowModalS(false);
        setSelectedItem(null);
    };


    if (!llrepuestos) {
        ObtenerRepuestos(getToken()).then((info) => {
            //console.log(info);
            if (info.code == 401) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setRepuestos(info.info);
                console.log(info);
                setLlrepuestos(true);
            }
        });
    }


    const handleGuardar = () => {
        const aux = Number(presupuesto);
        var datos = {
            "fechaEstimada": fecha,
            "presupuesto": aux,
            "descripcion": descripcion,
            "external_id": selectedItem.external_id
        };
        GuardarOrdenTrabajo(datos, getToken()).then((info) => {
            if (info.code == 200) {
                setSelectedItem(null);
                setDescripcion('');
                setFecha(new Date());
                setPresupuesto(0);
                mensajes('OK', 'Ok')
            } else {
                mensajes(info.msg, 'error');
                setLlauto(false);

            }
            setShowModalE(false);
        });
        setLlauto(false);
        navegation("/sesion")
    }

    const handleGuardarFactura = () =>{
        
        const detalle = [];
        for (let i = 0; i < selectedValues.length; i++) {
            const objeto ={
                "external_repuesto": selectedValues[i],
                "numero" :numeroRepuestosF[i]
                
            }     
            detalle.push(objeto);       
        }
        ObtenerOrden(selectedItem.external_id, getToken()).then((info)=>{
            var data ={
                "manoObra": manoObraF,
                "descripcion": descripcionF,
                "subtotal": sumaSubtotal,
                "total": sumaTotal,
                "external_orden": info.info[0].external_id,
                "detalle": detalle
            }
            console.log(data);
            GuardarFacturaServicio(data, getToken()).then((info) => {
                if (info.code == 200) {
                    setSelectedItem(null);
                    setDescripcionF('');
                    setManoObraF(0);
                    setSubtotalF(0);
                    setTotalF(0);
                    setNumeroRepuestosF([]);
                    setSelectedValues([]);
                    setCantidadRepuestosF(0);
                    
                } else {
                    mensajes(info.msg, 'error');
                    setLlauto(false);
                    
                }
                setShowModalS(false);
            });
            setLlauto(false);
            navegation("/sesion")
        })

        

    }

    AutosMecanica(getToken()).then((info) => {
        if (!llauto) {
            if (info.error == true && info.msg == 'Acceso denegado. Token a expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                setData(info.info);
            }
            setLlauto(true);
        }
    });

    const columnsV = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Año',
            selector: row => row.anio,
        },
        {
            name: 'Dueño',
            selector: row => row.nombres + " " + row.apellidos,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Placa',
            selector: row => row.placa,
        },
        {
            name: 'Marca',
            selector: row => row.nombre,
        },
        {
            name: 'Estado',
            selector: row => row.estado,
        },
        {
            name: 'Acciones',
            cell: (row) => {
                if (row.estado === 'VENDIDO') {
                    return (
                        <Button type="button" onClick={() => handleButtonClickE(row)} variant="warning">
                            Entrada
                        </Button>
                    );
                } else {
                    return (
                        <Button type="button" onClick={() => handleButtonClickS(row)} variant="warning">
                            Salida
                        </Button>
                    );
                }
            },
        },
    ];

    const encontrado = (index) => {
        const busqueda = repuestos.find(repuesto => repuesto.external_id === selectedValues[index]);
        const encontrado = Number(busqueda ? busqueda.precio : '');
        return encontrado;
    }

    const sumarRepuestos = () => {
        const n = cantidadRepuestosF;
        var suma = 0;
        for (let i = 0; i < n; i++) {
            const busqueda = repuestos.find(repuesto => repuesto.external_id === selectedValues[i]);
            const encontrado = Number(busqueda ? busqueda.precio : '') ;
            suma = suma + encontrado * numeroRepuestosF[i];
        }
        return suma;
    }

    const renderComboboxes = () => {
        const comboboxes = [];
        for (let i = 0; i < cantidadRepuestosF; i++) {
            comboboxes.push(
                <div key={i}>
                    <label >Respuesto</label>{i + 1}
                    <br />
                    <input
                        type="number"
                        min="1"
                        value={numeroRepuestosF[i]}
                        onChange={(e) => handleSpinnerChange(e, i)}
                    />
                    <br />
                    <select
                        value={selectedValues[i] || ''}
                        onChange={(e) => handleComboboxChange(e, i)}
                    >
                        <option value="">Seleccionar</option>
                        {repuestos.map((aux, i) => {
                            return (<option key={i} value={aux.external_id}>
                                {aux.nombre}
                            </option>)
                        })}
                    </select>
                    <input
                        value={encontrado(i) * numeroRepuestosF[i]}
                    />
                    <br />
                    <br />
                </div>
            );
        }
        return comboboxes;
    };

    const handleSpinnerChange = (e, index) => {
        const updatedValues = [...numeroRepuestosF];
        updatedValues[index] = e.target.value;
        setNumeroRepuestosF(updatedValues);
    };


    const handleComboboxChange = (e, index) => {
        const updatedValues = [...selectedValues];
        updatedValues[index] = e.target.value;
        setSelectedValues(updatedValues);
    };

    var sumaSubtotal = (Number(manoObraF) + Number(subtotalF) + sumarRepuestos());
    var sumaTotal = (sumaSubtotal*1.12).toFixed(2);


    return (
        <div className="container">
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                {/* Contenido del componente */}
                <DataTable
                    columns={columnsV}
                    data={data}
                />
                <Modal show={showModalE} onHide={closeModalE}>
                    <Modal.Header closeButton>
                        <Modal.Title>Entrada vehículo consecionario</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {selectedItem && (
                            <div>
                                <div className='container-fluid'>
                                    <div className="col-lg-10">
                                        <div className="p-6">
                                            <label>
                                                Fecha estimada de entrega:
                                                <input className="form-control form-control-user" placeholder="Ingrese la fecha"
                                                    type="date"
                                                    value={fecha}
                                                    onChange={(e) => setFecha(e.target.value)}
                                                />
                                            </label>
                                            <br />
                                            <label>
                                                Descripcion:
                                                <input
                                                    className="form-control form-control-user" placeholder="Ingrese una descripcion"
                                                    type="text"
                                                    value={descripcion}
                                                    onChange={(e) => setDescripcion(e.target.value)}
                                                />
                                            </label>
                                            <br />

                                            <label>
                                                Presupuesto:
                                                <input
                                                    className="form-control form-control-user" placeholder="Ingrese el presupuesto"
                                                    type="number"
                                                    value={presupuesto}
                                                    onChange={(e) => setPresupuesto(e.target.value)}
                                                />
                                            </label>
                                            <br />
                                            <br />
                                            <button className="btn btn-facebook btn-user btn-block" onClick={handleGuardar}>Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeModalE}>
                            Cerrar
                        </Button>
                    </Modal.Footer>
                </Modal>


                <Modal show={showModalS} onHide={closeModalS}>
                    <Modal.Header closeButton>
                        <Modal.Title>Salida vehículo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <div className='container-fluid'>
                                <div className="col-lg-10">
                                    <div className="p-6">
                                        <label>
                                            Precio mano de obra:
                                            <input className="form-control form-control-user" placeholder="Ingrese la fecha"
                                                type="number"
                                                min="1"
                                                value={manoObraF}
                                                onChange={(e) => setManoObraF(e.target.value)}
                                            />
                                        </label>
                                        <br />
                                        <label>
                                            Descripcion:
                                            <input className="form-control form-control-user" placeholder="Ingrese una descripcion"
                                                type="text"
                                                value={descripcionF}
                                                onChange={(e) => setDescripcionF(e.target.value)}
                                            />
                                        </label>
                                        <br />
                                        <label>
                                            Cantidad de repuestos:
                                            <input className="form-control form-control-user" placeholder="Ingrese la fecha"
                                                type="number"
                                                min="1"
                                                value={cantidadRepuestosF}
                                                onChange={(e) => setCantidadRepuestosF(e.target.value)}
                                            />
                                        </label>
                                        <br />
                                        {renderComboboxes()}
                                        <label>
                                            Subtotal:
                                            <input className="form-control form-control-user" placeholder=""
                                                type="label"
                                                value={sumaSubtotal}
                                                onChange={(e) => setSubtotalF(e.target.value)}
                                                readOnly={true}
                                            />
                                        </label>
                                        <br />
                                        <label>
                                            Total:
                                            <input className="form-control form-control-user" placeholder=""
                                                type="label"
                                                value={sumaTotal}
                                                onChange={(e) => setTotalF(e.target.value)}
                                                readOnly={true}
                                            />
                                        </label>
                                        <br />

                                        <br />
                                        <button className="btn btn-facebook btn-user btn-block" onClick={handleGuardarFactura}>Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeModalS}>
                            Cerrar
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        </div>
    );
}
