'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models/');
var cuenta = models.cuenta;
let jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const saltRounds = 8;

class CuentaController{
    async sesion(req, res){
        console.log(req.body);
        let errors = validationResult(req);
        console.log(req.body);
        if (errors.isEmpty()) {
            var login = await cuenta.findOne({where:{username: req.body.email},include: { model: models.persona, as: "persona", attributes: ['nombres', 'apellidos'] }});
            if (login === null) {
                res.status(200);
                res.json({ msg: "No existe la cuenta", code:200 , errors: errors });
            } else {
                res.status(200);
                var isClaveValida = function(clave, claveUser){
                    return bcrypt.compareSync(claveUser, clave);
                };
                if(login.estado){
                    if (isClaveValida(login.clave, req.body.clave)) {
                        const tokenData = {
                            external: login.external_id,
                            email: login.correo,
                            check: true
                        };
                        require('dotenv').config();
                        const llave = process.env.KEY;
                        const token = jwt.sign(tokenData, llave, {
                            expiresIn: '12h'
                        });
                        res.json({
                            msg: "OK",
                            token: token,
                            user: login.persona.nombres +' '+login.persona.apellidos,
                            correo: login.correo,
                            code: 200
                        })
                    }else{
                        res.json({ msg: "Clave incorrecta", code:200 });
                    }
                }else{
                    res.json({ msg: "Cuenta desactivada", code:200 });
                }

            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}

module.exports = CuentaController;