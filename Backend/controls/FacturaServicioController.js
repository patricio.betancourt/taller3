'use strict';
var models = require('../models/');
var repuesto = models.repuesto;
var detalleOrden = models.detalleOrden;
const { body, validationResult, check } = require('express-validator');
const auto = models.auto;
const ordenTrabajo = models.ordenTrabajo;
const facturaServicio = models.facturaServicio;


class FacturaServicioController {

    async listar(req, res) {
        var lista = await facturaServicio.findAll({ attributes: ['numero', 'fechaIngreso', 'fechaSalida', 'external_id', 'descripcion', 'total'] });
        res.json({ msg: "Lista", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let ordenTrabajo_id = req.body.external_orden;
            if (ordenTrabajo_id != undefined) {
                let ordenTrabajoAux = await ordenTrabajo.findOne({ where: { external_id: ordenTrabajo_id } });
                let autoAux = await auto.findOne({where:{id: ordenTrabajoAux.id_auto}});
                if (ordenTrabajoAux) {
                    var tamanio = await facturaServicio.count();
                    var id_aux = tamanio + 1;

                    const detalle = [];
                    const detalleAux = req.body.detalle;
                    const n = detalleAux.length;

                    for (let i = 0; i < n; i++) {
                        var repuesto_id = detalleAux[i].external_repuesto;
                        var numero = detalleAux[i].numero;
                        var repuestoAux = await repuesto.findOne({ where: { external_id: repuesto_id } });

                        const objeto = {
                            id_repuesto: repuestoAux.id,
                            id_facturaServicio: id_aux,
                            numero: numero,
                        };
                        detalle.push(objeto);
                    }

                    console.log(detalle);
                    const fechaActual = new Date().toISOString().split('T')[0];
                    var data = {
                        id: id_aux,
                        numero: id_aux,
                        fechaSalida: fechaActual,
                        fechaIngreso: ordenTrabajoAux.fechaIngreso,
                        descripcion: req.body.descripcion,
                        manoObra: req.body.manoObra,
                        subtotal: req.body.subtotal,
                        total: req.body.total,
                        detalleOrden: detalle,
                        id_ordenTrabajo: ordenTrabajoAux.id
                    };

                    console.log(data);
                    res.status(200);

                    let transaction = await models.sequelize.transaction();
                    try {
                        await facturaServicio.create(data, { include: [{ model: models.detalleOrden, as: "detalleOrden" }], transaction });
                        auto.update({estado:'VENDIDO'}, {where:{id: autoAux.id}});
                        await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async contar(req, res) {
        var numero = await facturaServicio.count();
        res.json({ msg: "Ok", code: 200, info: numero });
    }


}

module.exports = FacturaServicioController;