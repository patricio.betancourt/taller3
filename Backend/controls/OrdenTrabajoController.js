'use strict';
var models = require('../models/');
var ordenTrabajo = models.ordenTrabajo;
var auto = models.auto;
const { body, validationResult, check } = require('express-validator');

class ordenTrabajoController {
    async listar(req, res) {
        var lista = await ordenTrabajo.findAll({ attributes: ['fechaIngreso', 'fechaEstimada', 'external_id', 'descripcion', 'presupuesto'] });
        res.json({ msg: "Lista vacia", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let auto_id = req.body.external_id;
            if (auto_id != undefined) {
                let autoAux = await auto.findOne({ where: { external_id: auto_id } });
                let numeroAux = await ordenTrabajo.count();
                numeroAux = numeroAux+1;
                const fechaActual = new Date().toISOString().split('T')[0];
                var data = {
                    numero: numeroAux,
                    fechaIngreso: fechaActual,
                    fechaEstimada: req.body.fechaEstimada,
                    presupuesto: req.body.presupuesto,
                    descripcion: req.body.descripcion,
                    id_auto: autoAux.id
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();
                try {
                    await ordenTrabajo.create(data, { transaction });
                    auto.update({estado:'EN MECANICA'}, {where:{id: autoAux.id}});
                    console.log('guardado');
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });

                } catch (error) {

                    if (transaction) await transaction.rollback();
                    if (error.error && error.error[0].message) {
                        res.json({ msg: error.error[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos invalidos", code: 400, errors: errors });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async contar(req, res) {
        var numero = await ordenTrabajo.count();
        res.json({ msg: "Ok", code: 200, info: numero });
    }

    async obtener(req, res) {
        const external = req.params.external;
        const lista = await auto.findOne({
            where: { external_id: external },
            include: [
              {
                model: ordenTrabajo,
                as: 'ordenTrabajo',
                attributes: ['external_id'],
                limit: 1, // Limitar la asociación a un solo registro
                order: [['createdAt', 'DESC']], // Ordenar por la columna 'createdAt' en orden descendente
              },
            ],
            order: [['createdAt', 'DESC']], // Ordenar por la columna 'createdAt' en orden descendente
          });

        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista.ordenTrabajo });
    }

}

module.exports = ordenTrabajoController;