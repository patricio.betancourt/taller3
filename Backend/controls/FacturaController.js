'use strict';
var models = require('../models/');
var factura = models.factura;
const { body, validationResult, check } = require('express-validator');
const auto = models.auto;
const persona = models.persona;
const detalleFactura = models.detalleFactura;

class FacturaController {
    
    async listar(req, res) {
        var lista = await factura.findAll({
            include: { model: models.persona, as: "persona", attributes: ['identificacion'] },
            include: { model: models.detalleFactura, as: "detalleFactura", attributes: ['external_id']},
            attibutes: ['numero', 'fecha', 'total']
        });
        res.json({ msg: "Ok", code: 200, info: lista });

    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let persona_id = req.body.external_persona;
            let auto_id = req.body.external_auto;

            if (persona_id != undefined && auto_id!= undefined) {
                let personaAux = await persona.findOne({ where: { external_id: persona_id } });
                let autoAux = await auto.findOne({ where: { external_id: auto_id } });
                console.log(personaAux && autoAux);

                if (personaAux && autoAux) {

                    var data = {
                        numero: req.body.numero,
                        lugar: req.body.lugar,
                        subtotal: req.body.subtotal,
                        total: req.body.total,
                        fecha: req.body.fecha,
                        id_persona: personaAux.id,
                        detalleFactura :{
                            numero: req.body.cantidad
                        }
                    };
                    
                    res.status(200);
                    let transaction = await models.sequelize.transaction();

                    try {
                        await factura.create(data, {include: [{ model: models.detalleFactura, as: "detalleFactura" }], transaction});
                        
                        let facturaAux = await factura.findOne({ where: { numero: req.body.numero } , transaction});
                        if (autoAux === null) {
                            res.status(400);
                            res.json({ msg: "No existen registros que coincidan", code: 400 });
                        } else {
                            
                        }
                        console.log('guardado');
                        await transaction.commit();
                        var uuid = require('uuid');
                            console.log(facturaAux);
                            let detalleFacturaAux = await detalleFactura.findOne({ where: { id_factura: facturaAux.id } }, transaction);

                            var result = await auto.update({
                                id_detalleFactura: detalleFacturaAux.id,
                                external_id : uuid.v4(),
                                duenio : personaAux.identificacion,
                                estado : 'VENDIDO'
                            },{where:{id: autoAux.id}}
                            );
                            if (result === null) {
                                res.status(400);
                                res.json({ msg: "No se ha podido modificar sus datos", code: 400 });
                            } else {

                            }
                        res.json({ msg: "Se han regiustrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.error && error.error[0].message) {
                            res.json({ msg: error.error[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                            console.log(error);
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({
                    msg: "Faltan datos", code: 400
                });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}

module.exports = FacturaController;