'use strict';
var models = require('../models/');
var marca = models.marca;
const { body, validationResult, check } = require('express-validator');

class MarcaController {
    
    async listar(req, res) {
        var lista = await marca.findAll({
            attributes: ['nombre', 'external_id']
        });
        res.json({ msg: "Lista vacia", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
                var data = {
                    nombre: req.body.nombre
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();

                try {
                    await marca.create(data, {transaction });
                    console.log('guardado');
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.error && error.error[0].message) {
                        res.json({ msg: error.error[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async contar(req, res){
        var numero = await marca.count();
        res.json({msg: "Ok", code: 200, info: numero});
    }
}

module.exports = MarcaController;