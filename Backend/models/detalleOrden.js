'use strict';
module.exports = (sequalize, DataTypes) => {
    const detalleOrden = sequalize.define('detalleOrden', {
        numero: {type: DataTypes.INTEGER(20), defaultValue: 0 ,allowNull: false},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
        
    }, {freezeTableName: true});
    detalleOrden.associate = function(models){
        detalleOrden.belongsTo(models.facturaServicio, {foreignKey: 'id_facturaServicio', as: 'facturaServicio'});
        detalleOrden.belongsTo(models.repuesto, {foreignKey: 'id_repuesto', as: 'repuesto'});
    }
    return detalleOrden;
}