'use strict';
module.exports = (sequalize, DataTypes) => {
    const auto = sequalize.define('auto', {
        modelo: {type: DataTypes.STRING(50), defaultValue: "NO_DATA",allowNull: false,},
        placa: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        anio: {type: DataTypes.INTEGER, defaultValue: 0,allowNull: false,},
        color: {type: DataTypes.STRING(50), defaultValue: "NO_DATA",allowNull: false},
        precio: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        estado: {
            type: DataTypes.ENUM,
            values: ['DISPONIBLE','VENDIDO','EN MECANICA'],
            allowNull: false,
            defaultValue: 'DISPONIBLE'
        },
        cilindraje: {type: DataTypes.INTEGER, defaultValue: 0},
        duenio: {type: DataTypes.STRING(20),defaultValue: "NO_DATA"},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, {freezeTableName: true});
    auto.associate = function(models){
        auto.belongsTo(models.detalleFactura, {foreignKey: 'id_detalleFactura', as: 'detalleFactura'});
        auto.belongsTo(models.marca, {foreignKey: 'id_marca', as: 'marca'});
        auto.hasMany(models.ordenTrabajo, {foreignKey: 'id_auto', as: 'ordenTrabajo'})
    }
    return auto;
}