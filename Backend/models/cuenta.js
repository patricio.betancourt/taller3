'use strict';
module.exports = (sequalize, DataTypes) => {
    const cuenta = sequalize.define('cuenta', {
        username: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        clave: {type: DataTypes.STRING(120), defaultValue: "NO_DATA"},
        
        correo: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}
    }, {freezeTableName: true});
    cuenta.associate = function(models){
        cuenta.belongsTo(models.persona, {foreignKey: 'id_persona', as: 'persona'});
    }
    return cuenta;
}