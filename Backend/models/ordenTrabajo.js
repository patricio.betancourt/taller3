'use strict';
module.exports = (sequalize, DataTypes) => {
    const ordenTrabajo = sequalize.define('ordenTrabajo', {
        numero: {type: DataTypes.INTEGER(20), allowNull: false, unique: true},
        fechaIngreso: {type: DataTypes.DATEONLY, defaultValue: sequalize.NOW ,allowNull: false},
        fechaEstimada: {type: DataTypes.DATEONLY, defaultValue: sequalize.NOW ,allowNull: false},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        presupuesto: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        descripcion: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
        
    }, {freezeTableName: true});
    ordenTrabajo.associate = function(models){
        ordenTrabajo.belongsTo(models.auto, {foreignKey: 'id_auto', as: 'auto'});
        ordenTrabajo.hasOne(models.facturaServicio, {foreignKey: 'id_ordenTrabajo', as: 'facturaServicio'});
    }
    return ordenTrabajo;
}