'use strict';
module.exports = (sequalize, DataTypes) => {
    const repuesto = sequalize.define('repuesto', {
        nombre: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        precio: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        tipo: {
            type: DataTypes.ENUM,
            values: ['SISTEMA DE REFRIGERACION', 'SISTEMA DE ALIMENTACION DE GASOLINA', 'SISTEMA DE DIRECCION', 'SISTEMA DE SUSPENSION','SISTEMA ELECTRICO','SISTEMA DE TRANSMISION','SISTEMA DE ESCAPE','MOTOR DE GASOLINA','SISTEMA DE FRENADO','OTRO'],
            allowNull: false 
        },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        descripcion: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
    }, {freezeTableName: true});
    repuesto.associate = function(models){
        repuesto.hasMany(models.detalleOrden, {foreignKey: 'id_repuesto', as: 'detalleOrden'});
    }
    return repuesto;
}