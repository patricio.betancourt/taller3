'use strict';
module.exports = (sequalize, DataTypes) => {
    const facturaServicio = sequalize.define('facturaServicio', {
        numero: {type: DataTypes.INTEGER(20), allowNull: false, unique: true},
        fechaIngreso: {type: DataTypes.DATE, defaultValue: sequalize.NOW ,allowNull: false},
        fechaSalida: {type: DataTypes.DATE, defaultValue: sequalize.NOW ,allowNull: false},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        descripcion: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
        manoObra: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        subtotal: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        iva: {type: DataTypes.DECIMAL(10,2), defaultValue: 12.0,allowNull: false},
        total: {type: DataTypes.DECIMAL(10,2), defaultValue: 0.0,allowNull: false},
        
    }, {freezeTableName: true});
    facturaServicio.associate = function(models){
        facturaServicio.belongsTo(models.ordenTrabajo, {foreignKey: 'id_ordenTrabajo', as: 'ordenTrabajo'});
        facturaServicio.hasMany(models.detalleOrden, {foreignKey: 'id_facturaServicio', as: 'detalleOrden'});
    }
    return facturaServicio;
}